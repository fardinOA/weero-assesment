import Head from "next/head";
import Image from "next/image";
import { Inter } from "next/font/google";
import styles from "@/styles/Home.module.css";
import About from "@/components/About";
import Testimonial from "@/components/Testimonial";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
    return (
        <div className=" overflow-hidden ">
            <Head>
                <title>Nexus Tech</title>
            </Head>
            <About />
            <Testimonial />
        </div>
    );
}
