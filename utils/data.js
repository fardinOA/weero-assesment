// import { carrent, jobit, tripguide } from "../public/assets";

export const serviceData = [
    {
        name: "Custom Software Development",
        category: ["web", "app", "dextop", "mobile"],
        description:
            "Tailor-made software solutions crafted to fit your unique business needs.",
    },
    {
        name: "Mobile App Development",
        category: ["mobile"],
        description:
            "Expert mobile app development services for iOS and Android platforms.",
    },
    {
        name: "Web Development",
        category: ["web"],
        description:
            "Creating powerful web applications and websites that drive business growth.",
    },
    {
        name: "UI/UX Design",
        category: ["web", "mobile", "others"],
        description:
            "Designing intuitive and user-friendly interfaces that enhance user engagement.",
    },
    {
        name: "Cloud Solutions",
        category: ["mobile", "web", "others"],
        description:
            "Leveraging cloud computing to drive scalability and efficiency for your business.",
    },
    {
        name: "E-commerce Development",
        category: ["mobile", "web"],
        description:
            "Building robust e-commerce solutions that help businesses drive sales and growth.",
    },
    {
        name: "Blockchain Development",
        category: ["mobile", "web"],
        description:
            "Building secure and scalable blockchain solutions for businesses in various industries.",
    },
    {
        name: "Cybersecurity",
        category: ["mobile", "web", "others"],
        description:
            "Providing comprehensive cybersecurity solutions to help businesses protect their valuable data and systems.",
    },
    {
        name: "DevOps",
        category: ["mobile", "web", "others"],
        description:
            "Accelerating software development and delivery by optimizing development and operations processes.",
    },
    {
        name: "QA and Testing",
        category: ["mobile", "web", "others"],
        description:
            "Ensuring high-quality software products through comprehensive testing and quality assurance services.",
    },
];

export const services = [
    {
        name: "Custom Software Development",
        description:
            "Tailor-made software solutions crafted to fit your unique business needs.",
    },
    {
        name: "Mobile App Development",
        description:
            "Expert mobile app development services for iOS and Android platforms.",
    },
    {
        name: "Web Development",
        description:
            "Creating powerful web applications and websites that drive business growth.",
    },
];

export const workData = [
    {
        projectName: "Lyceum Article (PWA Blog Application)",
        image: "/images/Lyceum-article-home.jpeg",
        category: "full stack",
        gitLink: "https://gitlab.com/fardinOA/blog-application",
        live: "https://lyceum-article.vercel.app/",
    },
    {
        projectName: "Lyceum (Social Media Application)",
        image: "/images/Lyceum-home.jpeg",
        category: "full stack",
        gitLink: "https://gitlab.com/fardinOA/lyceum-frontend",
        live: "https://lyceum-frontend.vercel.app/login",
    },

    {
        projectName: "Ecommerce Web Application",
        image: "/images/Ecommerce-home.jpeg",
        category: "full stack",
        gitLink: "https://gitlab.com/fardinOA/ecommerce-frontend",
        live: "https://ecommerce-frontend-fardinoa.vercel.app/",
    },
    {
        projectName: "React Conference",
        image: "/images/conference-home.jpeg",
        category: "frontend",
        gitLink: "https://github.com/FardinOA/hive-assesment",
        live: "https://hive-assesment.vercel.app/",
    },
    {
        projectName: "Shop",
        image: "/images/shop-home.jpeg",
        category: "frontend",
        gitLink: "https://gitlab.com/fardinOA/blog-application",
        live: "",
    },
    {
        projectName: "School Management System",
        image: "/images/school-home.jpeg",
        category: "full stack",
        gitLink: "https://gitlab.com/fardinOA/school-management-system",
        live: "",
    },
];

export const projects = [
    {
        name: "Car Rent",
        description:
            "Web-based platform that allows users to search, book, and manage car rentals from various providers, providing a convenient and efficient solution for transportation needs.",
        tags: [
            {
                name: "react",
                color: "blue-text-gradient",
            },
            {
                name: "mongodb",
                color: "green-text-gradient",
            },
            {
                name: "tailwind",
                color: "pink-text-gradient",
            },
        ],
        image: "/assets/carrent.png",
    },
    {
        name: "Job IT",
        description:
            "Web application that enables users to search for job openings, view estimated salary ranges for positions, and locate available jobs based on their current location.",
        tags: [
            {
                name: "react",
                color: "blue-text-gradient",
            },
            {
                name: "restapi",
                color: "green-text-gradient",
            },
            {
                name: "scss",
                color: "pink-text-gradient",
            },
        ],
        image: "/assets/jobit.png",
    },
    {
        name: "Trip Guide",
        description:
            "A comprehensive travel booking platform that allows users to book flights, hotels, and rental cars, and offers curated recommendations for popular destinations.",
        tags: [
            {
                name: "nextjs",
                color: "blue-text-gradient",
            },
            {
                name: "supabase",
                color: "green-text-gradient",
            },
            {
                name: "css",
                color: "pink-text-gradient",
            },
        ],
        image: "/assets/tripguide.png",
    },
];

export const testimonial = [
    {
        name: "Sara Lee",
        company_name: "Acme Co",
        image: "https://randomuser.me/api/portraits/women/4.jpg",

        points: [
            "I thought it was impossible to make a website as beautiful as our product, but Rick proved me wrong.",
        ],
    },
    {
        name: "Chris Brown",
        company_name: "DEF Corp",
        image: "https://randomuser.me/api/portraits/men/5.jpg",

        points: [
            "I've never met a web developer who truly cares about their clients' success like Rick does.",
        ],
    },
    {
        name: "Lisa Wang",
        company_name: "456 Enterprises",
        image: "https://randomuser.me/api/portraits/women/6.jpg",
        points: [
            "After Rick optimized our website, our traffic increased by 50%. We can't thank them enough!",
        ],
    },
];
