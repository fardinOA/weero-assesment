import { motion } from "framer-motion";
import React from "react";
import "react-vertical-timeline-component/style.min.css";
import { VerticalTimeline } from "react-vertical-timeline-component";
import TestimonialCard from "./TestimonialCard";
import { textVariant } from "@/utils/motion";
import { testimonial } from "@/utils/data";
import { SectionWrapper } from "../HOC";

const Testimonial = () => {
    return (
        <div className="bg-black bg-opacity-20 ">
            <motion.div variants={textVariant()}>
                <h2
                    className={` text-[#000000] pl-2 font-black md:text-[60px] sm:text-[50px] xs:text-[40px] text-[30px] `}
                >
                    Testimonials
                </h2>
            </motion.div>
            <div className="mt-20 overflow-hidden  flex flex-col">
                <VerticalTimeline>
                    {testimonial.map((experience, ind) => (
                        <TestimonialCard key={ind} experience={experience} />
                    ))}
                </VerticalTimeline>
            </div>
        </div>
    );
};

export default SectionWrapper(Testimonial, "Testimonial");
