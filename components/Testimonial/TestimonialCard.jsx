import Image from "next/image";
import React from "react";
import { VerticalTimelineElement } from "react-vertical-timeline-component";

const TestimonialCard = ({ experience }) => {
    return (
        <VerticalTimelineElement
            contentStyle={{ background: "#1d1836", color: "#fff" }}
            contentArrowStyle={{ borderRight: "7px solid #000000" }}
            iconStyle={{ background: experience.iconBg }}
            icon={
                <div className="   flex justify-center items-center w-full h-full  ">
                    <Image
                        src={experience.image}
                        height={100}
                        width={100}
                        alt={experience.company_name}
                        className=" w-[100%] h-[100%] rounded-full object-contain "
                    />
                </div>
            }
        >
            <div>
                <h3 className=" text-white text-[24px] font-bold ">
                    {experience.name}
                </h3>

                <p
                    style={{ margin: 0 }}
                    className="  text-[16px] font-semibold "
                >
                    {experience.company_name}
                </p>
            </div>
            <ul className=" mt-5 list-disc ml-5 space-y-2 ">
                {experience.points.map((point, ind) => (
                    <li
                        key={`exprerience-point-${ind}`}
                        className="   text-[14px] pl-1 tracking-wider "
                    >
                        {point}
                    </li>
                ))}
            </ul>
        </VerticalTimelineElement>
    );
};

export default TestimonialCard;
