import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";

const links = ["about us", "services", "products", "contact"];

const Navbar = () => {
    const [toggle, setToggle] = useState(false);
    const [active, setActive] = useState("about us");
    const router = useRouter();

    useEffect(() => {
        if (router.asPath == "/") setActive("about us");
        else {
            setActive(router.asPath.slice(1, router.asPath.length));
        }
    }, [router]);

    return (
        <div className=" bg-gray-100 p-4  h-[100px] flex justify-center items-center  ">
            <div className="flex justify-between w-full ">
                <div className=" flex-[30%] text-[25px] tracking-wider font-bold ">
                    NexusTech
                </div>
                <div className="flex-[70%]   flex justify-center  ">
                    <div className=" hidden  md:flex   md:w-[100%] lg:w-[60%] gap-[40px] ">
                        {links?.map((ele, ind) => (
                            <Link
                                // onClick={() => setActive(ele)}
                                href={`${ele == "about us" ? "/" : `${ele}`}`}
                                className={` ${
                                    active == ele
                                        ? "text-[tomato]"
                                        : "text-black"
                                } capitalize  cursor-pointer font-bold hover:border-b border-black  text-[18px] leading-[24px]`}
                                key={`navLink-${ele}-${ind}`}
                            >
                                {ele}
                            </Link>
                        ))}
                    </div>
                </div>

                <div className="sm:hidden flex flex-1 justify-end items-center">
                    {toggle ? (
                        <svg
                            onClick={() => setToggle(!toggle)}
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            strokeWidth={1.5}
                            stroke="currentColor"
                            className="w-8 h-8 cursor-pointer "
                        >
                            <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                d="M6 18L18 6M6 6l12 12"
                            />
                        </svg>
                    ) : (
                        <svg
                            onClick={() => setToggle(!toggle)}
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            strokeWidth={1.5}
                            stroke="currentColor"
                            className="w-8 h-8 cursor-pointer "
                        >
                            <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"
                            />
                        </svg>
                    )}

                    <div
                        className={`${
                            !toggle ? "hidden" : "flex"
                        } p-6  bg-gray-300 absolute top-20 right-0 mx-4 my-2 min-w-[140px] z-10 rounded-xl`}
                    >
                        <ul className="list-none flex justify-end items-start flex-col gap-4 ">
                            {links.map((nav, ind) => (
                                <Link
                                    href={`${
                                        nav == "about us" ? "/" : `${nav}`
                                    }`}
                                    key={`${ind}`}
                                    className={` capitalize   hover:translate-x-2 transition-all font-medium cursor-pointer text-[16px] `}
                                    onClick={() => {
                                        setToggle(!toggle);
                                    }}
                                >
                                    {nav}
                                </Link>
                            ))}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Navbar;
