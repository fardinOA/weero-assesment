import React from "react";

const Footer = () => {
    return (
        <div>
            <div className=" bg-code  h-[70vh] flex flex-col justify-center items-center bg-black text-white ">
                <div className=" space-y-4 w-full flex flex-col md:flex-row justify-around  ">
                    <div className=" mt-6 tracking-wider mx-auto space-y-2 ">
                        <h1 className="  font-bold text-[25px] tracking-wider ">
                            Company
                        </h1>
                        <p className=" text-[18px] capitalize  ">about us</p>
                        <p className=" text-[18px] capitalize  ">
                            why choose us
                        </p>
                        <p className=" text-[18px] capitalize  ">Testimonial</p>
                    </div>

                    <div className=" mx-auto space-y-2 ">
                        <h1 className=" font-bold text-[25px] tracking-wider ">
                            Resources
                        </h1>
                        <p className=" text-[18px] capitalize  ">
                            privacy policy
                        </p>
                        <p className=" text-[18px] capitalize  ">
                            Terms & condition
                        </p>
                        <p className=" text-[18px] capitalize  ">contact us</p>
                    </div>
                    <div className=" tracking-[.2em] mx-auto space-y-2 ">
                        <h1 className=" font-bold text-[25px]   ">NexusTech</h1>
                    </div>
                </div>

                <div className="w-full mt-16 relative ">
                    <p className=" bg-white  h-2 bg-opacity-50 "></p>
                    <p className="bg-[#0B2447] px-[5rem] absolute top-1/2  left-[50%] transform -translate-x-1/2 -translate-y-1/2 ">
                        Copyright @{new Date().getFullYear()}
                    </p>
                </div>
            </div>
        </div>
    );
};

export default Footer;
