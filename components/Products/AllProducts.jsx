import { motion } from "framer-motion";
import React, { Suspense } from "react";
import ProductCard from "./ProductCard";
import { projects } from "@/utils/data";
import { SectionWrapper } from "../HOC";
import { textVariant } from "@/utils/motion";
import ImageLoader from "../ImageLoader";
const AllProducts = () => {
    return (
        <>
            <motion.div variants={textVariant()}>
                <h2
                    className={`  font-black md:text-[60px] sm:text-[50px] xs:text-[40px] text-[30px] `}
                >
                    Our Projects.
                </h2>
            </motion.div>
            <div className="flex w-full">
                <motion.p className=" mt-3 text-secondary text-[17px] max-w-3xl leading-[30px] ">
                    Our focus is on providing cutting-edge software solutions
                    that help businesses grow, expand their reach, and achieve
                    success in today&apos;s competitive digital landscape. We
                    are committed to delivering excellence in everything we do,
                    and our team is passionate about creating software that
                    makes a difference.
                </motion.p>
            </div>
            <Suspense
                fallback={
                    <ImageLoader
                        image={"/product-intro.jpg"}
                        alt={"product intro"}
                    />
                }
            >
                <div className=" bg-product-intro h-[20vh] md:h-[40vh] "></div>
            </Suspense>

            <div className="mt-20 flex flex-wrap gap-7">
                {projects.map((project, ind) => (
                    <ProductCard
                        ind={ind}
                        {...project}
                        key={`project-${ind}`}
                    />
                ))}
            </div>
        </>
    );
};

export default SectionWrapper(AllProducts, "products");
