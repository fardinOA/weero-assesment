import React from "react";
import Hero from "./Hero";
import AllProducts from "./AllProducts";

const Products = () => {
    return (
        <div>
            <Hero />
            <AllProducts />
        </div>
    );
};

export default Products;
