import React, { Suspense } from "react";
import ImageLoader from "../ImageLoader";

const Hero = () => {
    return (
        <Suspense
            fallback={
                <ImageLoader
                    image={"/products.jpg"}
                    alt={"products"}
                    height={70}
                />
            }
        >
            {" "}
            <div className="bg-products relative">
                <div
                    className=" space-y-4 text-[22px] w-[80%] md:text-[48px]  absolute top-1/2 left-[50%]   transform 
            -translate-x-1/2 -translate-y-1/2 "
                >
                    <p className="bg-white px-2   rounded-sm w-fit">
                        &quot;Unlock your potential with our software solutions.
                    </p>
                    <p className="bg-white px-2   rounded-sm w-fit">
                        Our software, your success.&quot;
                    </p>
                </div>
            </div>
        </Suspense>
    );
};

export default Hero;
