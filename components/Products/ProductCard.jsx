import { fadeIn } from "@/utils/motion";
import { motion } from "framer-motion";
import Image from "next/image";
import React from "react";

const ProductCard = ({ ind, name, description, tags, image }) => {
    return (
        <motion.div
            className="mx-auto w-full md:w-[inherit] "
            variants={fadeIn("up", "spring", ind * 0.5, 0.75)}
        >
            <div className=" bg-tertiary p-5  rounded-2xl sm:w-[360px] w-full ">
                <div className=" w-full relative h-[230px] ">
                    <Image
                        src={image}
                        alt={name}
                        height={200}
                        width={200}
                        layout="responsive"
                        className=" w-full  h-full object-cover rounded-2xl "
                    />
                </div>

                <div className="mt-5">
                    <h3 className=" text-white font-bold text-[24px] ">
                        {name}
                    </h3>
                    <p className=" mt-2 text-secondary text-[14px] ">
                        {description}
                    </p>
                </div>
                <div className="mt-4 flex flex-wrap gap-2">
                    {tags.map((tag, ind) => (
                        <p
                            key={tag.name}
                            className={` text-[14px] ${tag.color} `}
                        >
                            #{tag.name}
                        </p>
                    ))}
                </div>
            </div>
        </motion.div>
    );
};
export default ProductCard;
