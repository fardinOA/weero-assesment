import React, { Suspense } from "react";
import ImageLoader from "../ImageLoader";

const Hero = () => {
    return (
        <Suspense
            fallback={
                <ImageLoader image="/contact.jpg" alt={"contact"} height={80} />
            }
        >
            <div className="bg-contact"> </div>;
        </Suspense>
    );
};

export default Hero;
