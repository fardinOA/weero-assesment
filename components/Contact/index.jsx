import React, { useRef, useState } from "react";
import Hero from "./Hero";
import ContactForm from "./ContactForm";
import Head from "next/head";

const Contact = () => {
    return (
        <div>
            <Head>
                <title>Nexus Tech | Contact</title>
            </Head>
            <Hero />
            <div className=" bg-earth">
                <ContactForm />
            </div>
        </div>
    );
};

export default Contact;
