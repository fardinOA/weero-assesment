import React, { useRef, useState } from "react";
import { motion } from "framer-motion";
import { slideIn } from "@/utils/motion";
import { SectionWrapper } from "../HOC";

const ContactForm = () => {
    const formRef = useRef();
    const [form, setForm] = useState({
        name: "",
        email: "",
        message: "",
    });
    const [loading, setLoading] = useState(false);

    const handleChange = (e) => {
        const { name, value } = e.target;
        setForm({ ...form, [name]: value });
    };
    const handleSubmit = (e) => {
        e.preventDefault();
        setLoading(true);
        emailjs
            .send(
                "service_1apmano",
                "template_ntzywjy",
                {
                    from_name: form.name,
                    to_name: "Fardin Omor Afnan",
                    from_email: form.email,
                    to_email: "afnan.eu.cse@gmail.com",
                    message: form.message,
                },
                "qAMkvwhuW7wFKr83y"
            )
            .then(() => {
                setLoading(false);
                alert("Thank you. I will get back to you as soon as possible.");
                setForm({ name: "", email: "", message: "" });
            })
            .catch((err) => {
                setLoading(false);
                console.log(err);
                alert("Something went wrong");
            });
    };
    return (
        <div className="  flex flex-col xl:mt-12 xl:flex-row gap-10 overflow-hidden">
            <motion.div
                variants={slideIn("left", "tween", 0.2, 1)}
                className="    flex-[.75] bg-gray-100 p-8 rounded-2xl "
            >
                <p className="sm:text-[18px] text-[14px] text-secondary uppercase tracking-wider">
                    Get in touch
                </p>
                <p className=" font-black md:text-[60px] sm:text-[50px] xs:text-[40px] text-[30px]">
                    Contact.
                </p>

                <form
                    ref={formRef}
                    onSubmit={handleSubmit}
                    className="mt-12 flex flex-col gap-8"
                >
                    <label className="flex flex-col">
                        <span className=" font-medium mb-4">Your Name</span>
                        <input
                            name="name"
                            value={form.name}
                            onChange={handleChange}
                            placeholder="What's your name?"
                            type="text"
                            className="  py-4 px-6  rounded-lg outlined-none border-none "
                        />
                    </label>

                    <label className="flex flex-col">
                        <span className=" font-medium mb-4">Your Email</span>
                        <input
                            name="email"
                            value={form.email}
                            onChange={handleChange}
                            placeholder="What's your email?"
                            type="text"
                            className="  py-4 px-6  rounded-lg outlined-none border-none "
                        />
                    </label>

                    <label className="flex flex-col">
                        <span className=" font-medium mb-4">Your Message</span>
                        <textarea
                            rows="7"
                            name="message"
                            value={form.message}
                            onChange={handleChange}
                            placeholder="What do you want to say?"
                            type="text"
                            className="  py-4 px-6  rounded-lg outlined-none border-none "
                        />
                    </label>

                    <button
                        className=" cursor-pointer hover:translate-x-2 transition-all duration-300 py-3 px-8 outline-none w-fit  font-bold shadow-md shadow-primary rounded-xl "
                        type="submit"
                    >
                        {loading ? "Sending" : "Send"}
                    </button>
                </form>
            </motion.div>
            <motion.div
                variants={slideIn("right", "tween", 0.2, 1)}
                className=" xl:flex-1 xl:h-auto md:h-[550px] h-[350px] "
            ></motion.div>
        </div>
    );
};

export default SectionWrapper(ContactForm, "contact-form");
