import Image from "next/image";
import React from "react";

const ImageLoader = ({ image, alt, height }) => {
    return (
        <Image
            src={image}
            alt={alt}
            height={100}
            width={100}
            className={` h-[${height}vh] w-full `}
        />
    );
};

export default ImageLoader;
