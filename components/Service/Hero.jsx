import React, { Suspense } from "react";
import ImageLoader from "../ImageLoader";

const Hero = () => {
    return (
        <Suspense
            fallback={
                <ImageLoader
                    image="/services.jpg"
                    alt={"services"}
                    height={100}
                />
            }
        >
            <div className="bg-image-service   relative ">
                <div
                    className=" space-y-4 text-[22px] w-[80%] md:text-[48px]  absolute top-1/2 left-[50%]   transform 
            -translate-x-1/2 -translate-y-1/2 "
                >
                    <p className="bg-white px-2   rounded-sm w-fit">
                        &quot;Empowering Your Business with Technology.
                    </p>
                    <p className="bg-white px-2   rounded-sm w-fit">
                        From Concept to Creation -
                    </p>
                    <p className="bg-white px-2   rounded-sm w-fit">
                        We&apos;ve Got You Covered.&quot;
                    </p>
                </div>
            </div>
        </Suspense>
    );
};

export default Hero;
