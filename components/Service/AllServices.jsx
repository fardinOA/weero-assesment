import { motion } from "framer-motion";
import React, { useEffect, useMemo, useState } from "react";
import ServiceCard from "../About/ServiceCard";
import { fadeIn, textVariant } from "@/utils/motion";
import { serviceData } from "@/utils/data";
import { SectionWrapper } from "../HOC";

const AllServices = () => {
    const [category, setCategory] = useState();
    const [data, setData] = useState([]);

    function getFilteredList() {
        if (!category || category == "all") {
            return serviceData;
        }
        return data.filter((item) => item.category.includes(category));
    }

    let filteredList = getFilteredList();

    useEffect(() => {
        setData(serviceData);
    }, []);

    return (
        <div className="  ">
            <div className=" mt-12 bg-[#A8A6A1] rounded-[20px]">
                <div className="bg-[#6b6977] flex justify-between text-white rounded-2xl min-h-[300px] ">
                    <motion.div variants={textVariant()}>
                        <h2 className="text-white ml-2 font-black md:text-[60px] sm:text-[50px] xs:text-[40px] text-[30px]">
                            Our Services.
                        </h2>
                    </motion.div>

                    <div className="font-bold h-fit py-1 px-2">
                        <select
                            onChange={(e) => setCategory(e.target.value)}
                            id="countries"
                            className="bg-gray-50   text-gray-900 text-sm rounded-lg  block w-full p-2.5  "
                        >
                            <option defaultValue="all" value="all">
                                All
                            </option>
                            <option value="web">Web Development</option>
                            <option value="mobile">Mobile App</option>
                            <option value="others">Others</option>
                        </select>
                    </div>
                </div>
                <div className=" sm:px-16 px-6 -mt-20 pb-14 flex flex-wrap justify-around gap-7    ">
                    {filteredList?.map((service, ind) => (
                        <motion.div
                            key={`service-${ind}`}
                            initial="hidden"
                            whileInView="visible"
                            viewport={{ once: true }}
                            transition={{ duration: 0.3 }}
                            variants={{
                                visible: { opacity: 1, scale: 1 },
                                hidden: { opacity: 0, scale: 0 },
                            }}
                            className="  bg-[#2d2b38] mx-auto p-10 rounded-3xl  w-[320px]  l "
                        >
                            <p className="text-white font-black text-[48px] ">
                                {">"}
                            </p>

                            <div className="mt-1">
                                <p className=" text-white tracking-wider text-[18px] ">
                                    {service?.description}
                                </p>
                                <div className="mt-7  ">
                                    <div className=" ">
                                        <p className=" text-white font-medium text-[22px] ">
                                            <span className=" text-blue-400 ">
                                                @
                                            </span>
                                            {service?.name}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </motion.div>
                    ))}
                </div>
            </div>
        </div>
    );
};

export default SectionWrapper(AllServices, "servicePage");
