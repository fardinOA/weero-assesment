import React, { Suspense } from "react";
import Hero from "./Hero";
import AllServices from "./AllServices";
import Image from "next/image";
import Link from "next/link";
import ImageLoader from "../ImageLoader";
import Head from "next/head";

const Service = () => {
    return (
        <div>
            <Head>
                <title>Nexus Tech | Service</title>
            </Head>
            <Hero />
            <AllServices />
            <Suspense
                fallback={
                    <ImageLoader
                        image={"/not-convenced.jpg"}
                        alt={"not convenced"}
                        height={50}
                    />
                }
            >
                <div className="bg-not-convenced relative ">
                    <div
                        className=" space-y-4 text-[22px]   md:text-[48px]  absolute top-1/2 left-[50%]   transform 
            -translate-x-1/2 -translate-y-1/2 "
                    >
                        <p className=" bg-white px-2 rounded-sm ">
                            Not Convenced Yet!
                        </p>
                    </div>
                </div>
            </Suspense>
            <div className=" h-[70vh] md:h-screen  flex justify-center items-center ">
                <div className="flex  relative justify-between w-full md:w-[978px] h-[646px]  ">
                    <div className="w-full    flex items-center ">
                        <Image
                            src={"/see-product.jpg"}
                            alt={"see product"}
                            height={100}
                            width={100}
                            layout="responsive"
                            className=" object-contain w-full   "
                        />
                    </div>
                    <div className="w-full"></div>
                    <div className="  text-[18px] font-bold w-[70%]  md:text-[44px] md:space-y-3   absolute top-1/2 left-[60%] md:left-[70%] transform -translate-x-1/2 -translate-y-1/2 ">
                        <p className="bg-white rounded-sm w-fit px-2">
                            &quot;Take a look at our products
                        </p>
                        <p className=" animate-pulse  ">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                                strokeWidth={1.5}
                                stroke="currentColor"
                                className="w-9 h-9 rotate-180   "
                            >
                                <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M15.042 21.672L13.684 16.6m0 0l-2.51 2.225.569-9.47 5.227 7.917-3.286-.672zM12 2.25V4.5m5.834.166l-1.591 1.591M20.25 10.5H18M7.757 14.743l-1.59 1.59M6 10.5H3.75m4.007-4.243l-1.59-1.59"
                                />
                            </svg>
                        </p>
                        <p className="bg-blue-200 rounded-sm  w-fit px-2 cursor-pointer hover:translate-x-1 transition-all duration-300 ">
                            <Link href={`/products`}>All Products&quot;</Link>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Service;
