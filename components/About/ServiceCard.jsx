import { fadeIn } from "@/utils/motion";
import { motion } from "framer-motion";
import Image from "next/image";
import React from "react";

const ServiceCard = ({ ind, name, description }) => {
    return (
        <motion.div
            variants={fadeIn("", "spring", ind * 0.2, 0.75)}
            className="  bg-[#2d2b38] mx-auto p-10 rounded-3xl  w-[320px]  l "
        >
            <p className="text-white font-black text-[48px] ">{">"}</p>

            <div className="mt-1">
                <p className=" text-white tracking-wider text-[18px] ">
                    {description}
                </p>
                <div className="mt-7  ">
                    <div className=" ">
                        <p className=" text-white font-medium text-[22px] ">
                            <span className=" text-blue-400 ">@</span>
                            {name}
                        </p>
                    </div>
                </div>
            </div>
        </motion.div>
    );
};

export default ServiceCard;
