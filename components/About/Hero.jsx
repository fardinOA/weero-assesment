import Image from "next/image";
import React from "react";

const Hero1 = ({ imageSrc, imageAlt, quot1, quot2 }) => {
    return (
        <div className=" md:h-screen flex justify-center items-center ">
            <div className="flex  relative justify-between w-full md:w-[978px] h-[646px]  ">
                <div className="w-full    flex items-center ">
                    <Image
                        src={imageSrc}
                        alt={imageAlt}
                        height={100}
                        width={100}
                        layout="responsive"
                        className=" object-contain w-full   "
                    />
                </div>
                <div className="w-full"></div>
                <div className="  text-[18px] font-bold w-[70%]  md:text-[44px] space-y-3   absolute top-1/2 left-[60%] md:left-[70%] transform -translate-x-1/2 -translate-y-1/2 ">
                    <p className="bg-white rounded-sm w-fit px-2">{quot1}</p>
                    <p className="bg-white rounded-sm w-fit px-2 ">{quot2}</p>
                </div>
            </div>
        </div>
    );
};

export default Hero1;
