import { motion } from "framer-motion";
import React from "react";
import ServiceCard from "./ServiceCard";
import { textVariant } from "@/utils/motion";
import { SectionWrapper } from "../HOC";
import { services } from "@/utils/data";
import Link from "next/link";

const Services = () => {
    return (
        <div className=" mt-12 bg-[#A8A6A1] rounded-[20px]">
            <div className="bg-[#6b6977] flex justify-between text-white rounded-2xl min-h-[300px] ">
                <motion.div variants={textVariant()}>
                    <h2 className="text-white font-black md:text-[60px] sm:text-[50px] xs:text-[40px] text-[30px]">
                        Our Services.
                    </h2>
                </motion.div>
                <button className="  font-bold h-fit py-1 px-2 ">
                    <Link href={"/services"}> View all</Link>
                </button>
            </div>
            <div className=" sm:px-16 px-6 -mt-20 pb-14 flex flex-wrap justify-between gap-7    ">
                {services?.map((service, ind) => (
                    <ServiceCard key={service.name} ind={ind} {...service} />
                ))}
            </div>
        </div>
    );
};

export default SectionWrapper(Services, "services");
