import Image from "next/image";
import React from "react";

const Intro = () => {
    return (
        <div className=" bg-image-intro transition-all duration-500  relative  ">
            <div
                className=" w-[70%] space-y-4 text-[18px] md:text-[48px]  absolute top-1/2 left-[50%] transform 
            -translate-x-1/2 -translate-y-1/2 "
            >
                <p className="  bg-white px-2 rounded-sm w-fit   ">
                    Transforming your vision into reality
                </p>
                <p className="bg-white px-2 rounded-sm   w-fit">
                    Bridging the gap between imagination and implementation with
                    our software solutions.
                </p>
            </div>
        </div>
    );
};

export default Intro;
