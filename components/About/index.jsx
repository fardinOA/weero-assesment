import React, { useEffect, useState } from "react";
import Hero from "./Hero";
import Intro from "./Intro";

import Services from "./Services";
import { serviceData } from "@/utils/data";

const About = () => {
    return (
        <div>
            <Hero
                imageSrc={`/business.jpg`}
                imageAlt={"business"}
                quot1={`NexusTech is a dynamic and innovative software company based in Dhaka, Bangladesh. Founded in 2018, our mission is to provide end-to-end IT solutions to businesses of all sizes, with a particular focus on software development.`}
            />
            <Intro />

            <Hero
                imageSrc={`/happyClient.jpg`}
                imageAlt={"happyClient"}
                quot1={`If you're looking for a reliable and experienced software development partner, look no further than NexusTech. Contact us today to learn more about our services and how we can help your business succeed.`}
            />
            <Services hello="hello" serviceData={serviceData} />
        </div>
    );
};

export default About;
