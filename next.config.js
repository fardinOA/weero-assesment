/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: true,
    images: {
        domains: ["cdn.pixabay.com", "randomuser.me", "*"],
    },
};

module.exports = nextConfig;
